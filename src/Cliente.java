public class Cliente {
    /*************
     * Atributos
     *************/
    private String nombre;
    private String apellido;
    private String celular;
    private String cedula;

    /***************
     * Constructor
     **************/
    public Cliente(String nombre, String apellido, String cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

    /****************
     * Consultores
     * (Getters)
     **************/
    public String getNombre(){
        return this.nombre;
    }
    public String getApellido(){
        return this.apellido;
    }
    public String getCelular(){
        return this.celular;
    }
    public String getCedula(){
        return this.cedula;
    }

    /****************
     * Modificadores
     * (Setters)
     **************/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setApellido(String apellido){
        this.apellido = apellido;
    }
    public void setCelular(String celular){
        this.celular = celular;
    }
    public void setCedula(String cedula){
        this.cedula = cedula;
    }

}
