public class Taller {
    public static void main(String[] args) throws Exception {
        Recepcionista objRecepcionista = new Recepcionista("Ana", "Quintana", "123456");
        //Asignarle un sueldo
        objRecepcionista.setSueldo(1200.0);
        //Registrar un cliente
        objRecepcionista.registrar_cliente("Juan", "Perea", "98765", "3245678");
    }
}

/*******************
 * 
 * https://gitlab.com/mision_tic_2022/utp/ciclo2_2021/p16/taller
 * 
 * 
 */