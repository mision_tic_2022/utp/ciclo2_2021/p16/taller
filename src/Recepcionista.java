import java.util.ArrayList;

public class Recepcionista extends Empleado{
    /***********
     * Atributos
     ************/
    private ArrayList<Cliente> clientes;
    /***************
     * Constructor
     **************/
    public Recepcionista(String nombre, String apellido, String cedula){
        //Enviar datos a la super clase
        super(nombre, apellido, cedula);
        this.clientes = new ArrayList<Cliente>();
    }

    /**************
     * Métodos
     *************/
    public void registrar_cliente(String nombre, String apellido, String cedula, String celular){
        //Aquí construir el objeto Cliente
        Cliente objCliente = new Cliente(nombre, apellido, cedula);
        objCliente.setCelular(celular);
        clientes.add(objCliente);
        
    }
    
    public void registrar_vehiculos(){

    }
}
