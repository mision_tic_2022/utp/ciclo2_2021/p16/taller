/**
 * Autor: 
 * Empresa:
 * Descripción del proyecto:
 * Fecha:
 * Ciudad:
 */
public class Empleado {
    /************
     * Atributos
     ***********/
    private String nombre;
    private String apellido;
    private String cedula;
    private Double sueldo;

    /***************
     * Constructor
     **************/
    public Empleado(String nombre, String apellido, String cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.sueldo = 0.0;
    }

    /*****************
     * Modificadores
     ****************/
    public void setSueldo(Double sueldo){
        this.sueldo = sueldo;
    }
    
}
